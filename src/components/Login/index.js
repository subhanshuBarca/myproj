import React from 'react'
import { useState } from 'react';
import { Form, Button } from 'react-bootstrap'
import { useHistory } from 'react-router-dom';
import API from '../../api/api';


export default function Login(props) {
    const [isValid, setIsValid] = useState(true);
    const [mobileNumber, setMobileNo] = useState('');
    const history = useHistory();

    const setMobileData = (ev) => {
        setMobileNo(ev.target.value);
        if (ev.target.value) {
            let newNumStr = String(ev.target.value);
            let firstDig = newNumStr.substring(0, 1);
            if ((Number(firstDig) === 6 || Number(firstDig) === 7 || Number(firstDig) === 8 || Number(firstDig) === 9) && newNumStr.length === 10) {
                setIsValid(true);
            }
            else {
                setIsValid(false);
            }
        }
    }

    const getOtp = async (event) => {
        event.preventDefault();
        history.push('/otp');
        const resp = await API.post('/sendOtp', null, {
            params: {
                mobileNo: mobileNumber
            },
            headers: {
                'transactionId': 'react_interview',
            },
        });
        const appData = {
            "Mobile_Number": mobileNumber
        }

        localStorage.setItem("appData", JSON.stringify(appData));
        props.mobile(mobileNumber);
        // console.log(resp.data);
    }

    return (
        <div className="d-flex justify-content-center align-items-center container">
            <div className="card py-5 px-3 row">
                <div className="col">
                    <Form onSubmit={getOtp}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control type="tel" placeholder="Enter mobile no" onChange={(ev) => setMobileData(ev)} value={mobileNumber} maxLength='10' minLength='10' />
                            <Form.Text className="text-muted" className={isValid ? 'd-none' : 'd-block'}>
                                Please enter correct number
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword" className="d-none">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" />
                        </Form.Group>
                        <Button variant="primary" type="submit" disabled={!isValid || mobileNumber === '' ? true : false}>
                            Submit
                        </Button>
                    </Form>
                </div>
            </div>
        </div>
    )
}
