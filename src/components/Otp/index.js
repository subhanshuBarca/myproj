import React from 'react';
import { useState, useRef, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import API from '../../api/api';

export default function Otp(props) {
    // const appData = localStorage.getItem('appData');
    // const customerDetails = JSON.parse(appData);
    const [customerDetails, setCustomerDetails] = useState('');
    const [counter, setCounter] = useState(30);
    const [counterStart, setCounterStart] = useState(props.counterCheck);
    const [disabledBox, setDisabledBox] = useState(false);
    const [notValidOtp, setNotValidOtp] = useState(false)
    const [value1, setValue1] = useState('')
    const [value2, setValue2] = useState('')
    const [value3, setValue3] = useState('')
    const [value4, setValue4] = useState('')
    const inp1 = useRef(null);
    const inp2 = useRef(null);
    const inp3 = useRef(null);
    const inp4 = useRef(null);
    const history = useHistory();
    const editInputbox1 = (ev) => {
        let str = String(ev.target.value);
        if (str.length >= 1) {
            setValue1(ev.target.value);
            inp2.current.focus();
        }
        else {
            inp1.current.focus();
            setValue1(ev.target.value);
        }

    }
    const editInputbox2 = (ev) => {
        let str = String(ev.target.value);
        if (str.length >= 1) {
            setValue2(ev.target.value);
            inp3.current.focus();
        }
        else {
            inp2.current.focus();
            setValue2(ev.target.value);
        }
    }
    const editInputbox3 = (ev) => {
        let str = String(ev.target.value);
        if (str.length >= 1) {
            setValue3(ev.target.value);
            inp4.current.focus();
        }
        else {
            inp3.current.focus();
            setValue3(ev.target.value);
        }
    }
    const editInputbox4 = (ev) => {
        let str = String(ev.target.value);
        if (str.length >= 1) {
            setValue4(ev.target.value);

        }
        else {
            setValue4(ev.target.value);
        }
    }

    useEffect(() => {
        if (customerDetails === '') {
            const appData = localStorage.getItem('appData');
            setCustomerDetails(JSON.parse(appData));
        }
        if (counterStart) {
            if (value1.length === 0) {
                inp1.current.focus();
            }
            else if (value2.length === 0) {
                inp2.current.focus();
            }
            else if (value3.length === 0) {
                inp3.current.focus();
            }
            else if (value4.length === 0) {
                inp4.current.focus();
            }
            counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
        }

    }, [counter])

    const handleKeypress = (ev) => {
        if (String(ev.which) === "13") {
            clickHandler();
        }
    }

    const clickHandler = async (ev) => {
        if (value1.length === 1
            && value2.length === 1
            && value3.length === 1
            && value4.length === 1
        ) {
            const otpData = `${value1}${value2}${value3}${value4}`;
            // console.log(otpData);
            const customer = JSON.parse(localStorage.getItem('appData'));
            const resp = await API.post('/verifyOtp', null, {
                params: {
                    mobileNo: customer['Mobile_Number'],
                    otp: Number(otpData),
                    deviceKey: "abcd",
                    isIos: false,
                    source: "react_interview"
                },
                headers: {
                    'transactionId': 'react_interview',
                },
            });
            if (resp[201]) {
                const customerDetails = {
                    "AddressList": resp['AddressList'],
                    "CustomerDto": resp['CustomerDto'],
                    "CustomerId": resp['CustomerId'],
                    "PaymentList": resp['PaymentList'],
                    "isNewCustomer": resp['isNewCustomer'],
                    "referArray": resp['referArray'],
                    "Mobile_Number": customer['Mobile_Number']
                }
                localStorage.setItem('appData', JSON.stringify(customerDetails));
                sessionStorage.setItem('token', JSON.stringify(resp['Response']['access_token']))
                setDisabledBox(false);
                history.push('/get-details');
                setNotValidOtp(false);
            }
            else {
                setNotValidOtp(true);
            }
        }
        else {
            setDisabledBox(true);
        }
    }
    const resendClickHandler = async (ev) => {
        setValue1('');
        setValue2('');
        setValue3('');
        setValue4('');
        inp1.current.focus();
        await API.post('/sendOtp', null, {
            params: {
                mobileNo: customerDetails.Mobile_Number
            },
            headers: {
                'transactionId': 'react_interview',
            },
        });
        setCounter(30);
    }

    return (
        <div>
            <div className="d-flex justify-content-center align-items-center container">
                <div className="card py-5 px-3">
                    <h5 className="m-0">Mobile phone verification</h5><span className="mobile-text">Enter the code we just send on your mobile phone {customerDetails.Mobile_Number}<b className="text-danger"></b></span>
                    <div className="d-flex flex-row mt-5">
                        <input
                            type="number"
                            className="form-control"
                            value={value1}
                            onChange={editInputbox1}
                            onKeyPress={handleKeypress}
                            ref={inp1}
                        />
                        <input
                            type="number"
                            className="form-control"
                            value={value2}
                            onChange={editInputbox2}
                            onKeyPress={handleKeypress}
                            ref={inp2}
                        />
                        <input
                            type="number"
                            className="form-control"
                            value={value3}
                            onChange={editInputbox3}
                            onKeyPress={handleKeypress}
                            ref={inp3}
                        />
                        <input
                            type="number"
                            className="form-control"
                            value={value4}
                            onChange={editInputbox4}
                            onKeyPress={handleKeypress}
                            ref={inp4}
                        />
                    </div>
                    {disabledBox ? <p style={{ textAlign: "center", color: "red" }} className="mt-2">Text box is not completed!</p> : null}
                    {notValidOtp ? <p style={{ textAlign: "center", color: "red" }} className="mt-2">Please enter valid otp!</p> : null}
                    <Button variant="primary" type="submit" onClick={clickHandler} className="mt-5">
                        Submit
                    </Button>
                    <div className="text-center mt-3" style={{ color: "red" }}>Countdown: {counter}</div>
                    <div className="text-center mt-3">
                        <span className="d-block mobile-text">Don't receive the code?</span>
                        <Button variant="warning" className="mt-3" disabled={counter === 0 ? false : true} onClick={resendClickHandler}>
                            <span className="font-weight-bold text-danger cursor" >Resend</span>
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    )
}
