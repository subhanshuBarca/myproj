import React, { useState } from 'react'
import { useEffect } from 'react'
import API from '../../api/api';
import axios from "axios";
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { createBrowserHistory } from 'history';

export default function Getarticles() {
    const [timer, setTimer] = useState(60);
    const [articles, setArticles] = useState([]);
    const [category, setCategory] = useState([]);
    const history1 = createBrowserHistory();
    const history = useHistory();
    const resetTimer = ()=>{
        setTimer(60);
    }
    const logOut=()=>{
        localStorage.clear();
        sessionStorage.clear();
        history.push('/');
    }

    useEffect(async () => {
        timer > 0 && setTimeout(() => setTimer(timer - 1), 1000);
        if (timer === 0) {
            const token = JSON.parse(sessionStorage.getItem('token'));
            const resp = await axios.post('https://stage-services.truemeds.in/ArticleService/getArticleListing', null, { headers: { "Authorization": `Bearer ${token}` } })
            if (resp) {
                if (resp['status'] == 200) {
                    setArticles(resp['data']['result']['article']);

                    setCategory(resp['data']['result']['category']);
                }
            }
        }
        console.log(history);
        history1.replace({ pathname: '/get-details' })

    }, [timer])
    
    return (
        <div className="container">
            <div className="row">
                <div className="col">
                    <div style={{ color: 'red' }} className="text-center">00:{timer >= 10 ? timer : ('0' + timer)}</div>
                    <div className="text-center">
                        <Button variant="warning" className="mt-3" disabled={timer === 0 ? false : true}>
                            <span className="font-weight-bold text-danger cursor" onClick={resetTimer}>Reset</span>
                        </Button>
                        <Button variant="primary" type="submit" className="mt-3 ml-2" onClick={logOut}>
                            Logout
                        </Button>
                    </div>
                </div>
            </div>
            <div className="row mt-5">
                <div className="col">
                    {category.map((data) =>
                        <div key={data.id} className="card my-2">
                            <div  className="card-title text-center my-2"><b>{data.name}</b></div>
                        </div>
                    )}
                </div>
            </div>
            <div className="row mt-5">
                <div className="col">
                {articles.map((data) =>
                        <div key={data.id} className="card mx-3 my-3">
                            <div  className="card-title mx-1"><b>Doctor Name: </b>{data.author}</div>
                            <div  className="card-title mx-1"><b>Disease: </b>{data.categoryName}</div>
                            <div  className="card-title mx-1"><b>causes :</b>{data.name}</div>
                            <img className="card-img" src={data.image}/>
                            <div  className="card-footer mx-1">
                            <a href={data.url} target="_blank"><b>check details</b></a></div>

                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}
