import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import './App.css';
import Login from './components/Login';
import Otp from './components/Otp';
import Getarticles from './components/Getarticles';
import { useEffect, useState } from 'react';


function App() {
  const [mobileNo, setMobileNo]= useState('')
  const [token, setToken]= useState('')
  const editMobileNo = (mobile)=>{
    console.log(mobile);
    setMobileNo(mobile);
  }
  useEffect(() => {
    if(token === ''){
      setToken(JSON.parse(sessionStorage.getItem('token')));
    }
  }, [])
  return (
    <div className="App">
      <header className="text-center py-2 px-3">
        <h1>Our Articles</h1>
      </header>
      <Router >
        <Switch>
          <Route exact path="/">
            <Login mobile={editMobileNo} />
          </Route>
          <Route path="/otp">
            <Otp counterCheck={true} mobileNumber= {mobileNo}/>
          </Route>
          <Route path = "/get-details">
            {token !== ''?<Getarticles />:null}
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
