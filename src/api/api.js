import axios from "axios";
import ClipLoader from "react-spinners/ClipLoader";




const axios1 = axios.create({
  baseURL: "https://stage-services.truemeds.in/CustomerService/",
  
  

});
axios1.interceptors.request.use((req) => handleRequest(req));
axios1.interceptors.response.use((res) => handleResponse(res), (rej) => handleError(rej));

function check(){
  <div>
    <ClipLoader  loading={true}  size={150} />
  </div>
}

function handleRequest(req) {
  check()
  // const token = localStorage.getItem('MOB-2-TKN');
  // req.headers.Authorization =  'Bearer ' + token;
  return req;
}
const handleError = (error) => {
  error = { ...error };
  if (error.code) {
    return undefined;
  }
  const response = error.response;
  if (!response) {
    return undefined;
  }
  const status = response.status;
  const message = response.data.message;

  return error.response.data;
}
function handleResponse(res){
  const status = res.status;
  const message = res.data.message || 'Success';
  return res.data;
}

export default axios1;